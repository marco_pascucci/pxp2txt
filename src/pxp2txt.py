import igor.packed as ig
import numpy as np
import os, sys
import re


def get_records(root):
    found_records = []
    for e in root:
        if not isinstance(e,str):
            s = e.decode('ascii')
            m = re.match(r'Record.+', s)
            if m: found_records.append(m.string)
    return found_records


def return_record(filename,r_name):
    records, filesystem = ig.load(filename)
    root = filesystem['root']
    Record = root[r_name.encode('ascii')]
    wave = Record.wave['wave']
    wData = wave['wData']
    SampleInterval = root[b'SampleInterval']
    x = np.arange(0,len(wData))*SampleInterval
    y = np.array(wData)
    return x,y


def list_records(filename):
    try:
        extension = filename.split('.')[1]
    except:
        extension = ''

    if extension != 'pxp' :
        print("wrong file extension, pxp expected")
        sys.exit(1)

    records, filesystem = ig.load(filename)
    root = filesystem['root']

    records = get_records(root)

    return records



def convert_folder(folder, all_records=True):
    filenames = os.listdir(folder)

    for filename in filenames:

        try:
            extension = filename.split('.')[1]
        except:
            extension = ''

        if extension != 'pxp' : continue

        # Read PXP file
        records, filesystem = ig.load(folder+filename)

        # select Root folder
        root = filesystem['root']

        # scan the records
        records = get_records(root)

        if not all_records: records = records[0:1]

        for r in records:
            # select first record
            Record = root[r.encode('ascii')]
            header = Record.header

            # select the Record's wave
            wave = Record.wave['wave']

            # wave name
            bname = wave['wave_header']['bname']
            name = bname.decode('ascii')

            # data
            wData = wave['wData']

            # out = np.array([x,wData]).T
            out = np.array(wData).T
            np.savetxt(folder + filename.split('.')[0]+'_' + r + '.txt', out, delimiter='\t')

            print(filename, 'record \"'+ r + '\" processed')

        # sample interval ?
        SampleInterval = root[b'SampleInterval']
        x = np.arange(0,len(wData))*SampleInterval
        try:
            np.savetxt(folder+filename+'_abscisse.txt', x.T)
            print("time vector saved")
        except:
            print("No file processed")



if __name__ == "__main__":
    import re,os

    def print_usage():
        usage = "pxp2txt usage:\n" \
                "pxp2txt -all [folder] : convert all folder content, all found records\n"\
                "pxp2txt -first [folder] : convert all folder content, only first record of each PXP file\n"\
                "pxp2txt -list [pxp file] : list record in a file\n"\
                "pxp2txt -plot [pxp file] : plot a record in a file (interactive)\n" \
                "pxp2txt -plotall [pxp file] : plot all records in a file\n"
        print(usage)

    # reverse the argument list
    argv = list(reversed(sys.argv[1:]))

    if len(argv) != 2 :
        print_usage();
        sys.exit(1)
    else:

        arg = argv.pop()

        if arg == '-all':
            folder = argv.pop()
            if not os.path.isdir(folder):
                print("invalid path")
                sys.exit(1)
            else:
                if folder[-1] != os.sep: folder = folder + os.sep
                print("Working on floder:",  folder, '\n')
                convert_folder(folder,all_records=True)
                print('\nwork done')

        if arg == '-first':
            folder = argv.pop()
            if not os.path.isdir(folder):
                print("invalid path")
                sys.exit(1)
            else:
                if folder[-1] != os.sep: folder = folder + os.sep
                print("Working on floder:",  folder)
                print("decoding only the first record", '\n')
                convert_folder(folder,all_records=False)
                print('\nwork done')

        elif arg == '-list':
            filename = argv.pop()
            if not os.path.isfile(filename):
                print("invalid file name")
                sys.exit(1)
            else:
                print("listing records in", filename,'\n')
                records = list_records(filename)
                for i,r in enumerate(records):
                    print(i,r,sep='\t')

        elif arg == '-plot':
            import matplotlib.pyplot as plt
            filename = argv.pop()
            if not os.path.isfile(filename):
                print("invalid file name")
                sys.exit(1)
            else:
                print("listing records in", filename,'\n')
                records = list_records(filename)
                for i,r in enumerate(records):
                    print(i,r,sep='\t')

                n = -1
                while n >= len(records) or n < 0:
                    try :
                        n = input('chose record number to plot[0]: ')
                        if n == "": n = 0
                        n = int(n)
                    except:
                        print("record number must be an integer")
                        n=-1

                    try:
                        r = records[n]
                    except:
                        print("wrong record number")

                x,y = return_record(filename,r)
                plt.plot(x,y,label=r)
                plt.show()

        elif arg == '-plotall':
            import matplotlib.pyplot as plt
            filename = argv.pop()
            if not os.path.isfile(filename):
                print("invalid file name")
                sys.exit(1)
            else:
                records = list_records(filename)
                for r in records:
                    x,y = return_record(filename,r)
                    plt.plot(x,y,label=r)
                plt.legend()
                plt.show()
# def test():
#     filename = '/home/marco/Remote/pxp2txt/test/jeu4fvr2016c2_081_power0p05.pxp'
#     records, filesystem = ig.load(filename)
#
#     root = filesystem['root']
#
#     found_records = []
#     for e in root:
#         if not isinstance(e,str):
#             s = e.decode('ascii')
#             m = re.match(r'Record.+', s)
#             if m: found_records.append(m.string)
#
#     for r in found_records:
#         print(r.encode('ascii'))
